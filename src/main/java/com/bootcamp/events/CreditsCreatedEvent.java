package com.bootcamp.events;

import com.bootcamp.infraestructure.document.Credits;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class CreditsCreatedEvent extends Event<Credits>{
}
