package com.bootcamp.infraestructure.dto;

import com.bootcamp.infraestructure.document.Credits;

/** Credits request entity. */
public class CreditsRequest extends Credits {}
