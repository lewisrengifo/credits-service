package com.bootcamp.infraestructure.repository;


import com.bootcamp.infraestructure.document.Credits;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/** Reactive repository for credits. */
@Repository
public interface CreditsRepository
    extends ReactiveMongoRepository<Credits, String>, CreditsCustomRepository {}
