package com.bootcamp.infraestructure.repository;


import com.bootcamp.infraestructure.document.Credits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Mono;

/** Implementation of CreditsCustomRepository. */
public class CreditsCustomRepositoryImpl implements CreditsCustomRepository {

  private final ReactiveMongoTemplate mongoTemplate;

  @Autowired
  public CreditsCustomRepositoryImpl(ReactiveMongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  @Override
  public Mono<Boolean> hasCredits(String document) {
    Query query = new Query(Criteria.where("document").is(document));
    return mongoTemplate.exists(query, Credits.class);
  }
}
