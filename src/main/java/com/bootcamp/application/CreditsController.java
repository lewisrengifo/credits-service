package com.bootcamp.application;

import com.bootcamp.infraestructure.document.Credits;
import com.bootcamp.infraestructure.dto.CreditsRequest;
import com.bootcamp.infraestructure.service.CreditsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

/** Rest controller for credits. */
@RestController
@RequestMapping("/credits")
public class CreditsController {

  @Autowired private CreditsService creditsService;

  @PostMapping("/save")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Credits> registerCredit(@RequestBody CreditsRequest credits) {
    return creditsService.save(credits);
  }

  @PutMapping("/payments/{id}")
  public Mono<Credits> registerPayment(
      @PathVariable String id, @RequestBody CreditsRequest creditsRequest) {
    return creditsService.registerPayments(id, creditsRequest);
  }

  @PutMapping("/consumption/{amount}")
  public Mono<Credits> registerConsumption(
      @PathVariable("amount") BigDecimal amount, Credits credits) {
    return creditsService.registerConsumption(credits, amount);
  }

  @GetMapping("/getAll")
  public Flux<Credits> readCredits() {
    return creditsService.getAll();
  }

  @DeleteMapping("/delete/{id}")
  public Mono<Credits> deleteById(@PathVariable("id") String id) {
    return creditsService.deleteById(id);
  }
}
