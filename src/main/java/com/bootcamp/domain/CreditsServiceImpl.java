package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.Credits;
import com.bootcamp.infraestructure.repository.CreditsRepository;
import com.bootcamp.infraestructure.service.CreditsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

/** Implementation for credits service interface. */
@Service
public class CreditsServiceImpl implements CreditsService {

  @Autowired private CreditsRepository creditsRepository;

  @Override
  public Flux<Credits> getAll() {
    return this.creditsRepository.findAll();
  }

  public static BigDecimal payments(BigDecimal total, BigDecimal payment) {
    return total.subtract(payment);
  }

  public static BigDecimal addConsumption(BigDecimal total, BigDecimal consumption) {
    return total.add(consumption);
  }

  @Override
  public Mono<Credits> save(Credits credits) {
    if (credits.getType().equals("personal")) {
      return creditsRepository
          .hasCredits(credits.getId())
          .flatMap(
              hasCredits -> {
                if (hasCredits) {
                  return Mono.empty();
                } else {
                  return this.creditsRepository.save(credits);
                }
              });
    }
    return this.creditsRepository.save(credits);
  }

  @Override
  public Mono<Credits> existByDocumentId(String documentId) {
    return this.creditsRepository.findById(documentId);
  }

  @Override
  public Mono deleteById(String clientId) {
    return creditsRepository.deleteById(clientId);
  }

  @Override
  public Mono<Credits> registerPayments(String id, Credits credits) {
    return findById(id)
        .flatMap(
            result -> {
              BigDecimal finalAmount = payments(result.getTotalAmount(), credits.getTotalAmount());
              credits.setTotalAmount(finalAmount);
              return this.creditsRepository.save(credits);
            });
  }

  @Override
  public Mono<Credits> registerConsumption(Credits credits, BigDecimal consumption) {
    return findById(credits.getId())
        .flatMap(
            result -> {
              BigDecimal finalAmount = addConsumption(credits.getTotalAmount(), consumption);
              if (finalAmount.compareTo(credits.getCreditLimit()) == 1) {
                return Mono.empty();
              } else {
                credits.setTotalAmount(finalAmount);
                return this.creditsRepository.save(credits);
              }
            });
  }

  @Override
  public Mono<Credits> findById(String id) {
    return this.creditsRepository.findById(id);
  }
}
