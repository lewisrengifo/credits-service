package com.bootcamp.domain;

import com.bootcamp.events.CreditsCreatedEvent;
import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.infraestructure.document.Credits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class CreditsEventsService {

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.credits.name:credits}")
  private String topicCredits;

  public void publish(Credits credits) {
    CreditsCreatedEvent createdEvent = new CreditsCreatedEvent();
    createdEvent.setData(credits);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicCredits, createdEvent);
  }
}
